<?php 
	class PostsController {
		
		
        public function add() {
            
            $posts = Post::manageDb();

            $error = array(true, true, true, true);

            if (isset($_POST['submit'])) {
                $_SESSION['select'] = $_POST['select'];
                if (empty($_POST['from'])) {
                    $_SESSION['errorFrom'] = "This field is required";
                    $error[0] = true;
                } else {
                    $error[0] = false;
                    unset($_SESSION['errorFrom']);
                }

                if (empty($_POST['email'])) {
                    $_SESSION['errorEmail'] = "This field is required";
                    $error[1] = true;
                } else {
                    $error[1] = false;
                    unset($_SESSION['errorEmail']);
                    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                        $_SESSION['errorEmail'] = "Invalid email format";
                        $error[1] = true;
                    }
                }

                if (empty($_POST['to'])) {
                    $_SESSION['errorTo'] = "This field is required";
                    $error[2] = true;
                } else {
                    $error[2] = false;
                    unset($_SESSION['errorTo']);
                }

                if (empty($_POST['recipient'])) {
                    $_SESSION['errorRecipient'] = "This field is required";
                    $error[3] = true;
                } else {
                    $error[3] = false;
                    unset($_SESSION['errorRecipient']);
                    if (!filter_var($_POST['recipient'], FILTER_VALIDATE_EMAIL)) {
                        $_SESSION['errorRecipient'] = "Invalid email format";
                        $error[3] = true;
                    }
                }

                if (!$error[0] AND !$error[1] AND !$error[2] AND !$error[3]) {
                    unset($_SESSION['errorFrom']);
                    unset($_SESSION['errorEmail']);
                    unset($_SESSION['errorTo']);
                    unset($_SESSION['errorRecipient']);
                    require_once('views/posts/summary.php');
                } else {
                    require_once('views/pages/main.php');
                }
            }
        }
		
	}
?>