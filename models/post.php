<?php 
	class Post {
		public $from;
		public $yourEmail;
		public $to;
		public $recipientEmail;
		public $voucher;
		public $message;

		public function __construct($from, $yourEmail, $to, $recipientEmail, $voucher, $message) {
			$this->from 			= $from;
			$this->yourEmail		= $yourEmail;
			$this->to 		 		= $to;
			$this->recipientEmail	= $recipientEmail;
			$this->voucher 			= $voucher;
			$this->message 			= $message;
		}

		public static function manageDb() {
			$db = Db::getInstance();
			$db->query('CREATE DATABASE IF NOT EXISTS PROJEKT');
			$db->query('use PROJEKT');
			$db->query('CREATE TABLE IF NOT EXISTS Dane ( 
						Id INT(3) NOT NULL PRIMARY KEY AUTO_INCREMENT,
						NameFrom VARCHAR(50) NOT NULL,
						YourEmail VARCHAR(100) NOT NULL,
						NameTo VARCHAR(100) NOT NULL,
						RecipientEmail VARCHAR(100) NOT NULL,
						Voucher VARCHAR(5),
						Message VARCHAR(200))');

			$from = $_POST['from'];
			$yourEmail = $_POST['email'];
			$to = $_POST['to'];
			$recipientEmail = $_POST['recipient'];
			$voucher = "£".$_POST['select'];
			$message = $_POST['message'];

			$sql = "INSERT into Dane (NameFrom, YourEmail, NameTo, RecipientEmail, Voucher, Message) VALUES('$from','$yourEmail','$to','$recipientEmail', '$voucher','$message')";
			
			$db->query($sql);

			$sql = "SELECT * FROM Dane WHERE Id = " . $db->lastInsertId() . "";

			$req = $db->query($sql);

			$result = $req->fetch();

			$lastRow = new Post($result['NameFrom'], $result['YourEmail'], $result['NameTo'], $result['RecipientEmail'], $result['Voucher'], $result['Message']);

			return $lastRow;
		}

		
	}
?>