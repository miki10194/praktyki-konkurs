<?php
session_start();

	require_once('connection.php');
	if (isset($_GET['controller']) && isset($_GET['action'])) {
		$controller = $_GET['controller'];
		$action = $_GET['action'];
	} else {
		$controller = 'pages';
		$action     = 'main';
	    if (isset($_SESSION['errorFrom'])) {
        	unset($_SESSION['errorFrom']);
    	}
	    if (isset($_SESSION['errorEmail'])) {
        	unset($_SESSION['errorEmail']);
    	}
	    if (isset($_SESSION['errorTo'])) {
        	unset($_SESSION['errorTo']);
    	}
	    if (isset($_SESSION['errorRecipient'])) {
        	unset($_SESSION['errorRecipient']);
    	}
	    if (isset($_SESSION['select'])) {
        	unset($_SESSION['select']);
    	}
	}

	require_once('views/layout.php');
?>