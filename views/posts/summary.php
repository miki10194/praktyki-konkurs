<section id=section-summary>
	<div id="summary">
	        <div class="gift-vouchers">GIFT VOUCHERS</div>
		<div class="image">
			<img src="images/logo.png">
		</div>
		<div class="image-select">
	        <img src="images/<?php echo "$posts->voucher";?>.png"\>
		</div>

		<div class="summary-title">
			<?php 	
				if (strcmp($posts->voucher, "£100" ) == 0) {
					echo "Congratulations successfully complete a form and choose a gift voucher $posts->voucher :), but Sorry, this voucher is currently unavailable ";
				} else
					echo "Congratulations successfully complete a form and choose a gift voucher $posts->voucher :)"; ?>
		</div>

		<div class="summary-table">
			<table>
				<tr>
					<th>From</th><th>Your Email</th><th>To</th><th>Recipient Email</th><th>Voucher</th><th>Message</th>
				</tr>
				<tr>
					<td><?php echo $posts->from; ?></td> 
					<td><?php echo $posts->yourEmail; ?></td> 
					<td><?php echo $posts->to; ?></td> 
					<td><?php echo $posts->recipientEmail; ?></td> 
					<td><?php 	
							if (strcmp($posts->voucher, "£100") == 0) {
								echo 'Unavailable';
							} else 
								echo $posts->voucher; ?></td> 
					<td><?php echo $posts->message; ?></td>
				</tr>
			</table>
			
			<div class="back-to-form">
				<input type="submit" name="submit" value="BACK TO FORM" onclick="location.href='/Projekt';">
			</div>
		</div>
	</div>
</section>