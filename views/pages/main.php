<div id="page">

	<header id="header">
        <div class="gift-vouchers">GIFT VOUCHERS</div>
        <div class="image"><img src="images/logo.png"></div>
        <div class="image-select">
            <?php
                if (isset($_SESSION['select'])) {
                    echo "<img id=\"image-select\" src=\"images/£".$_SESSION['select'].".png\"></div>";
                }else{
                    echo "<img id=\"image-select\" src=\"images/£50.png\"></div>";
                }
            ?>
    </header>


	<section id="section-main">
		<div class="title">
			This is a voucher that can be used for almost anything on our store-except Gift Vouchers or Subscription plans.
		</div>

		<div class="text-fields">
			<form method="POST" action="?controller=posts&action=add"> 

				<div class="from">
					<label for="from">From:</label> <span class="error">
					<?php 
						if(isset($_SESSION['errorFrom'])) { 
							echo $_SESSION['errorFrom']; }
					?></span>
				</div>

				<div class="input-from">
					<input id="from" type="text" name="from" placeholder="YOUR NAME HERE" maxlength="50" 
					value="<?php 
						if(isset($_POST['from'])) 
							echo $_POST['from'];
					?>"> 
				</div>

				<div class="your-email">
					<label for="email">Your email address:</label> <span class="error">
					<?php 
						if(isset($_SESSION['errorEmail'])) { 
							echo $_SESSION['errorEmail']; }
					?></span>
				</div>

				<div class="input-your-email">
					<input id="email" type="text" name="email" placeholder="YOUR EMAIL ADDRESS HERE" maxlength="100" 
					value="<?php 
						if(isset($_POST['email'])) 
							echo $_POST['email'];
					?>"> 
				</div>

				<div class="to">
					<label for="to">To:</label><span class="error">
					<?php 
						if(isset($_SESSION['errorTo'])) { 
							echo $_SESSION['errorTo'];}
					?></span>
				</div>

				<div class="input-to">
					<input id="to" type="text" name="to" placeholder="RECIPIENT'S NAME HERE" maxlength="100" 
					value="<?php 
						if(isset($_POST['to'])) 
							echo $_POST['to'];
					?>">
				</div>

				<div class="recipients-email">
					<label for="recipient">Recipient's email address:</label> <span class="error">
					<?php 
						if(isset($_SESSION['errorRecipient'])) { 
							echo $_SESSION['errorRecipient'];}
					?></span>
				</div>

				<div class="input-recipients-email">
					<input id="recipient" type="text" name="recipient" placeholder="RECIPIENT'S EMAIL ADDRESS HERE" maxlength="100" 
					value="<?php 
						if(isset($_POST['recipient'])) 
							echo $_POST['recipient'];
					?>">
				</div>

				<div class="faq">
					<label>FAQ's about our Gift Vouchers</label>
				</div>

				<div class="voucher-amount">
					<label>Voucher Amount:</label>
				</div>
				<div class="voucher-box">
					<select name="select" onchange="document.getElementById('image-select').src = 'images/£' + this.value + '.png'">
                        <?php
                        for ($i = 10; $i <= 100; $i = $i + 10) {
                            echo "<option value='" . $i . "'"; 

                            if (isset($_SESSION['select'])) {
                                if ($i == $_SESSION['select']) {
                                    echo "selected";
                                }
                            } else {
                            	if ($i == 50 ) 
                            		echo "selected";                           	
                            }
                            echo ">£" . $i . "</option>";
                        }
                        ?>
                    </select>
				</div>

				<div class="message">
					<label for="message">Message:</label>
				</div>

				<div class="message-text-area">
					<textarea id="message" name="message" placeholder="Hi, Frank,
Thinking of you with this voucher. Have fun.
From Dave" maxlength="200"><?php 
						if(isset($_POST['message'])) 
							echo $_POST['message'];
					?></textarea> 
				</div>

				<div class="buy-gift-voucher">
 					<input type="submit" name="submit" value="BUY GIFT VOUCHER">
				</div>

			</form>
		</div>


	</section>
</div>